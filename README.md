# Ipê

Minimal project that implements the requirements for
[Vio.com coding challenge](https://github.com/viodotcom/backend-assignment-elixir).

## Project name

Since the challenge relates to IP data, the name was selected due to the similarity with the word IP and to homage
the beautiful [Ipê](https://en.wikipedia.org/wiki/Handroanthus) tree, quite common in South America:

![Ipê](https://upload.wikimedia.org/wikipedia/commons/e/e8/Ipe_detail.jpg)

## Project Structure

- [IpeData](https://gitlab.com/altjohndev/ipe-data) is the library responsible to manage IP location resources.
- [IpeAPI](https://gitlab.com/altjohndev/ipe-api) is the REST JSON API web server.
- [IpeIaC](https://gitlab.com/altjohndev/ipe-iac) is the Infrastructure as Code.

## Report

### Discovery

Started by inspecting (using Livebook) the two data sources in order to extract valuable information
about the data to be processed. The livebook is available [here](discovery/livebooks/data_inspection.livemd).

### The Library

Generated the
[migration](https://gitlab.com/altjohndev/ipe-data/-/blob/main/priv/repo/migrations/20231014140325_create_ip_locations.exs)
and the [schema](https://gitlab.com/altjohndev/ipe-data/-/blob/main/lib/ipe_data/ip_location.ex) with
`create_changeset/1` function for sanitization and parsing. Related
[unit tests](https://gitlab.com/altjohndev/ipe-data/-/blob/main/test/ipe_data/ip_location_test.exs)
validated some use cases.

Set the [test repository](https://gitlab.com/altjohndev/ipe-data/-/blob/main/test/support/repo.ex)
and the architecture of the project.

The architecture isolates the data layer from the database layer through the natural usage of
[Ecto.Schema](https://hexdocs.pm/ecto/Ecto.Schema.html) for the data definition,
[Ecto.Changeset](https://hexdocs.pm/ecto/Ecto.Changeset.html) for changes, and
[Ecto.Repo](https://hexdocs.pm/ecto/Ecto.Repo.html) for database operations.

I decided to make the library database-agnostic by relying on dynamic repo settings, meaning that it can be used by
any service that can define any valid `Ecto.Repo` definition.

If, for example, a service requires connection with a MySQL database instead of (or alongside with) the traditional
PostgreSQL, it will simply require the definition of a new `Ecto.Repo` module to start using the operations
provided by the library.

The splitting of reader and manager modules for entity operations simplifies the usage and the maintenance of the
source code, specially if the related service requires read operations to go through a replica database instead of the
primary database (or any other kind of tenancy).

The [reader](https://gitlab.com/altjohndev/ipe-data/-/blob/main/lib/ipe_data/readers/ip_location_reader.ex)
exposes the function from which the API service will use to fetch the IP location. Related
[unit tests](https://gitlab.com/altjohndev/ipe-data/-/blob/main/test/ipe_data/readers/ip_location_reader_test.exs)
validated some use cases.

The [manager](https://gitlab.com/altjohndev/ipe-data/-/blob/main/lib/ipe_data/manager/ip_location_manager.ex) exposes
the function that can be used to import IP locations from a CSV file, either locally or remotely. Related
[unit tests](https://gitlab.com/altjohndev/ipe-data/-/blob/main/test/ipe_data/managers/ip_location_manager_test.exs)
validated some use cases. There is also one test case for
[cloud_data_dump.csv](https://gitlab.com/altjohndev/ipe-data/-/blob/main/test/ipe_data/managers/ip_location_manager_test.exs#L41-56)
and another for
[data_dump.csv](https://gitlab.com/altjohndev/ipe-data/-/blob/main/test/ipe_data/managers/ip_location_manager_test.exs#L58-69).

Finally, I created the CI integration, wrapped up the documentation and tests, and generated the git tag `v0.0.1`.

### The API

Bootstrapped the minimal Phoenix project, created the
[controller](https://gitlab.com/altjohndev/ipe-api/-/blob/main/lib/ipe_api/web/controllers/ip_location_controller.ex)
and the [route](https://gitlab.com/altjohndev/ipe-api/-/blob/main/lib/ipe_api/web/router.ex#L14),
[tested](https://gitlab.com/altjohndev/ipe-api/-/blob/main/test/ipe_api/web/controllers/ip_location_controller_test.exs)
with two simple cases and prepared the CI integration.

Then I set the [Dockerfile](https://gitlab.com/altjohndev/ipe-api/-/blob/main/Dockerfile) and published it to [Docker Hub](https://hub.docker.com/r/altjohndev/ipe-api).

A sample [docker-compose.yml](docker-compose.yml) can be used to deploy the service:

```bash
# Start services
docker-compose up -d

# Access API remote console
docker-compose exec api bin/ipe_api remote
```

In the remote console:

```elixir
# Import cloud_data_dump.csv
iex> IpeData.IPLocationManager.import_from_csv remote_csv_url: "https://raw.githubusercontent.com/viodotcom/backend-assignment-elixir/master/cloud_data_dump.csv"

# Or import the data_dump.csv
iex> IpeData.IPLocationManager.import_from_csv remote_csv_url: "https://raw.githubusercontent.com/viodotcom/backend-assignment-elixir/master/data_dump.csv"
```

Expected output:

```elixir
# For cloud_data_dump.csv
{:ok,
 %{
   started_at: ~N[2023-10-15 03:39:57.722224],
   inserted_amount: 17015,
   discarded_amount: 2984,
   downloaded_at: ~N[2023-10-15 03:39:58.106396],
   finished_at: ~N[2023-10-15 03:40:00.099506]
 }}

# For data_dump.csv
{:ok,
 %{
   started_at: ~N[2023-10-15 07:25:50.974183],
   inserted_amount: 849275,
   discarded_amount: 150725,
   downloaded_at: ~N[2023-10-15 07:25:56.842560],
   finished_at: ~N[2023-10-15 07:26:44.472744]
 }}
```

Access the HTTP endpoint with an existing IP address: <http://localhost:8080/ip-locations/200.106.141.15>

The expected result:

```json
{
  "data": {
    "ip_location": {
      "ip_address": "200.106.141.15",
      "country_code": "SI",
      "country": "Nepal",
      "city": "DuBuquemouth",
      "latitude": "-84.87503094689836",
      "longitude": "7.206435933364332"
    }
  },
  "errors": []
}
```

### The Deployment

For deployment, I created the [IaC](https://gitlab.com/altjohndev/ipe-iac) project to deploy an EC2 instance with
docker that starts the PostgreSQL and the API containers.

Sample address in the running instance: <http://ec2-3-77-202-56.eu-central-1.compute.amazonaws.com/ip-locations/200.106.141.15>

## Considerations

The algorithm designed to process the CSV is not optimal. It ensures low usage of memory and IO by processing lazily and
in chunks, which is enough for data sources similar to the two CSVs provided by the challenge.

The strategy designed to handle duplications is naive, since it can waste database resources by sending duplicated
records for processing with duplications across batches. The duplications are ignored by the database and it does not
affect the results.

Processing speed can be increased by:

- Parallel processing of rows (with OTP, Flow, or even clustering)
- Usage of CLI tools (such as [xsv](https://github.com/BurntSushi/xsv)) and/or languages with better IO/memory
  performance for processing (such as Rust)
- Seed through `\copy` with processed CSVs

The deployment approach with micro EC2 instance and docker-compose for both API and PostgreSQL is not suitable for
live usage.

The deployment could be automated through CD or simplified for teams through GitOps or ChatOps.

Both Elixir projects documentation could be published in a private or self-hosted Hex package manager, improving
development experience.

To visualize the documentation, you can use the following command inside any project:

```bash
mix docs --open --formatter html
```

![ipe_api_docs](images/ipe_api_docs.png)
![ipe_data_docs](images/ipe_data_docs.png)
